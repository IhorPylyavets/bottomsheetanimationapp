package com.example.bottomsheetanimation.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.bottomsheetanimation.R
import com.example.bottomsheetanimation.withAppBottomSheet

class SmallFragment: Fragment(R.layout.fragment_small) {
    companion object {
        fun newInstance() = SmallFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<View>(R.id.back_button)
            .setOnClickListener { withAppBottomSheet { goBack() } }
    }
}